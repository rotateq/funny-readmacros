;;;; euro-quote-reader.lisp

;;; In this file we want to show the capability of defining new readmacros.
;;; First we want reading in a string with '€"' in which control-strings
;;; can come up like in C#.
;;; So we define #\€ as a dispatch-macro-character and then set one with €".
;;; It will read through the next none escaped #\" as usual but parses out
;;; slots directly; later it can also do calculations inside.
;;; For the real thing we will use FORMAT of course to which it expands.
;;; Maybe it will be of high value using the numarg too in a later version
;;; but first getting the simple case right.

(cl:in-package #:cl-user)

(make-dispatch-macro-character #\€)

(defun euro-quote-reader (stream char1 char2)
  (declare (ignore char1 char2))
  (symbol-macrolet ((chr[] (make-array 0 :element-type 'character 
                                         :adjustable t 
                                         :fill-pointer t)))
    (let ((cat-vector '())
          (symbols '())
          (str chr[])
          (chr #\nul))
      (loop :named read-block
            :do (setq chr (read-char stream))
            :if (char= chr #\")
              :do (return-from read-block)
            :else :if (char= chr #\{)
              :do (push str cat-vector)
              :and :do (setq str chr[]))
            :else :if (char= chr #\})
              :do (push "~d" cat-vector)
              :and :do (push (intern (string-upcase str)) symbols)
              :and :do (setq str chr[]))
            :else 
              :do (vector-push-extend chr str))
      `(format nil ,(apply #'str:concat (reverse cat-vector)) ,@(reverse symbols)))))
